import { createApp, serveStatic } from "https://servestjs.org/@v1.1.6/mod.ts";
import yargs from "https://deno.land/x/yargs/deno.ts";
import { YargsType } from "https://deno.land/x/yargs/types.ts";

const { port, path, tlsCert, tlsKey } = yargs(Deno.args)
  .command(
    "start [path]",
    "start serving the files at the given path",
    (yargs: YargsType) => {
      return yargs
        .positional("path", {
          describe: "the path to static serve files from",
          default: ".",
        })
        .option("p", {
          alias: "port",
          default: 8080,
          describe: "port to serve the static files",
          type: "number",
        })
        .option("c", {
          alias: "tls-cert",
          describe: "path to TLS certificate file",
          type: "string",
        })
        .option("k", {
          alias: "tls-key",
          describe: "path to TLS key file",
          type: "string",
        });
    }
  )
  .strictCommands()
  .demandCommand(1).argv;

const app = createApp();

app.use(serveStatic(path));

if (tlsCert && tlsKey) {
  app.listenTls({
    certFile: tlsCert,
    keyFile: tlsKey,
    port,
  });
} else {
  app.listen({ port });
}
