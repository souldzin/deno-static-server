# Deno static server

A simple Deno script that starts a static server at the given path.

## Example - HTTP server

```
deno run --allow-net --allow-read "https://gitlab.com/souldzin/deno-static-server/-/raw/v1.1.0/cli.ts" start --port=1234
```

## Example - HTTPS server

```
deno run --allow-net --allow-read "https://gitlab.com/souldzin/deno-static-server/-/raw/v1.1.0/cli.ts" start ./path/to/files --port=1234 --tls-key="/path/to/key.pem" --tls-cert="/path/to/cert.pem"
```
