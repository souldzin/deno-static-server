DIR_ROOT=$(dirname $(dirname $0))
DIR_WWW=$DIR_ROOT/temp/www
DIR_DEMO=$DIR_ROOT/tests/fixture-demo-files
EXIT_CODE=0
PID=-1

# Arrange - setup fixture
mkdir -p $DIR_ROOT/temp
rm -r $DIR_WWW
cp -r $DIR_DEMO $DIR_WWW

# Arrange - start server
deno run --allow-net --allow-read $DIR_ROOT/cli.ts start $DIR_WWW --port=8080 & PID=$(echo $!)
sleep 30

# Act - request fixture file
curl "http://localhost:8080/hello.txt" > $DIR_WWW/output.txt

if cmp -s "$DIR_WWW/output.txt" "$DIR_DEMO/hello.txt" ; then
  echo "Test passed :)"
else
  echo "Test failed :("
  EXIT_CODE=1
fi

# Cleanup
kill $PID
rm -r $DIR_WWW

# Exit
exit $EXIT_CODE
